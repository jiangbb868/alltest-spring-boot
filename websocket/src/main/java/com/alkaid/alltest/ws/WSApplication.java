package com.alkaid.alltest.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WSApplication {

    public static void main(String[] args) {
        System.out.println("Welcome ...");
        SpringApplication.run(WSApplication.class, args);
    }
}
