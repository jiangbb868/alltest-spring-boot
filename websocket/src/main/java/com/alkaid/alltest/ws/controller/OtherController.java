package com.alkaid.alltest.ws.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/other")
public class OtherController {

    @GetMapping("/list")
    @ResponseBody
    public String list() throws Exception {
        return "hello world";
    }
}
