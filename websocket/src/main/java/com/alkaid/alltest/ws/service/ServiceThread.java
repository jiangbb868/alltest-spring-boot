package com.alkaid.alltest.ws.service;

import com.alkaid.alltest.ws.controller.WebsocketServer;

import java.util.Date;

public class ServiceThread extends Thread{

    private WebsocketServer server;

    public ServiceThread(WebsocketServer server) {
        this.server = server;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000000; i ++) {
            try {
                Thread.sleep(10000);
                server.sendMessage("hello " + new Date().toString());
            } catch (Exception e) {

            }
        }
    }
}
