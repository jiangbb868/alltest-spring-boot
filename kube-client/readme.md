# 通过https访问k8s的apiServer
```
curl https://172.17.162.71:6443/api/v1/nodes \
--cacert /etc/kubernetes/pki/ca.crt \
--cert /etc/kubernetes/pki/apiserver-kubelet-client.crt \
--key /etc/kubernetes/pki/apiserver-kubelet-client.key

cd D:\work\Alkaid\2019proj\alltest-spring-boot\kube-client\src\main\resources\ssl
curl https://172.17.162.71:6443/api/v1/nodes --cacert ca.crt --cert apiserver-kubelet-client.crt --key apiserver-kubelet-client.key


cd main/src/resources
curl https://172.17.162.235:6443/api/v1/nodes --cacert ca.crt --cert apiserver-kubelet-client.crt --key apiserver-kubelet-client.key
```
# 将k8s master节点 /etc/kubernetes/pki下的证书文件拷贝到 resources/ssl目录下

```
curl: (77) schannel: next InitializeSecurityContext failed: SEC_E_UNTRUSTED_ROOT (0x80090325) - 证书链是由不受信任的颁发机构颁发的。
https://www.cnblogs.com/tylerzhou/p/11102605.html

curl: (35) schannel: next InitializeSecurityContext failed: Unknown error (0x80092012) - 吊销功能无法检查证书是否吊销。

curl https://172.17.162.235:6443/api/v1/nodes --cacert D:\software\cloud\k8s-ssl\ca.crt --cert D:\software\cloud\k8s-ssl\apiserver-kubelet-client.crt --key D:\software\cloud\k8s-ssl\apiserver-kubelet-client.key
```

https://blog.csdn.net/zzq900503/article/details/88352902

java  -Djavax.servlet.request.encoding=UTF-8 -Dfile.encoding=UTF-8 -Duser.language=zh_CN -jar -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=172.17.162.71:8000 -jar kube-client-1.0-SNAPSHOT.jar