package com.alkaid.clickhouse.controller;

import com.alkaid.clickhouse.entity.Visits;
import com.alkaid.clickhouse.service.IVisitsService;
import com.alkaid.clickhouse.util.ClickhouseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Controller
public class VisitController {

    @Autowired
    private IVisitsService visitsService;
    @Autowired
    private DataSource dataSource;

    @GetMapping("/list")
    @ResponseBody
    public List<Visits> list() {
        List result = ClickhouseUtil.sqlQuery("select * from tb_metrics");
        return result;
    }

    @GetMapping("/listByDs")
    @ResponseBody
    public List<Visits> listByDs() throws SQLException {
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from tb_metrics");
        return null;
    }


    @GetMapping("/query/{sql}")
    @ResponseBody
    public List<Visits> list(@PathVariable("sql") String sql) {
        List result = ClickhouseUtil.sqlQuery(sql);
        return result;
    }
}
