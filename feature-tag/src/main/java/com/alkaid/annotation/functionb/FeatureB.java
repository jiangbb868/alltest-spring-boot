package com.alkaid.annotation.functionb;

import com.alkaid.annotation.annotation.FeatureTag;
import com.alkaid.annotation.base.AbstractFeature;

@FeatureTag(name ="feature-b", dependencies = {"feature-a"})
public class FeatureB extends AbstractFeature {
    @Override
    public boolean initBusiness(String tagName) {
        System.out.println("feature b initialize");
        return true;
    }
}
