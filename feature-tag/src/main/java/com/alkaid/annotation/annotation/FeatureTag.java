package com.alkaid.annotation.annotation;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FeatureTag {
    /**
     * 标签名称，唯一标识，必填
     * @return
     */
    String name();

    /**
     * 前置依赖标签，字符串数组，非必填，请仔细阅读下述文字再决定是否需要填写：
     * 特性标签依赖通常可由升级包的顺序来控制，这里就无需填写，如果升级包的顺序无法控制特性依赖时，才填写该字段，有如下场景
     * 1.同一个升级版本中，含有多个特性标签，之间存在依赖关系
     * 2.补丁包中含有新的特性
     * @return
     */
    String[] dependencies() default {};
}
