package com.alkaid.annotation.processor;

import com.alkaid.annotation.annotation.FeatureTag;
import com.alkaid.annotation.base.AbstractFeature;
import com.alkaid.annotation.base.Feature;
import lombok.SneakyThrows;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class AnnotationBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    List<FeatureTag> featureTags = new ArrayList<>();
    Map<String, FeatureTag> featureTagByNameMap = new HashMap<>();
    Map<String, Class<Feature>> tagFeatures = new HashMap<>();

    @SneakyThrows
    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        ClassPathScanningCandidateComponentProvider classPathScanningCandidateComponentProvider = new ClassPathScanningCandidateComponentProvider(false);
        classPathScanningCandidateComponentProvider.addIncludeFilter(new AnnotationTypeFilter(FeatureTag.class));
        Set<BeanDefinition> beanDefinitions = classPathScanningCandidateComponentProvider.findCandidateComponents("com.alkaid.annotation.*");
        List<FeatureTag> unOrderTags = new ArrayList<>();
        for(BeanDefinition beanDefinition : beanDefinitions){
            registry.registerBeanDefinition(beanDefinition.getBeanClassName(), beanDefinition);
            Class clazz = Class.forName(beanDefinition.getBeanClassName());
            if (clazz.equals(AbstractFeature.class)) {
                continue;
            }
            FeatureTag tag = (FeatureTag)clazz.getDeclaredAnnotation(FeatureTag.class);
            tagFeatures.put(tag.name(), clazz);
            featureTagByNameMap.put(tag.name(), tag);
            if (tag.dependencies().length == 0) {
                featureTags.add(tag);
            } else {
                unOrderTags.add(tag);
            }
        }

        while (!orderTag(unOrderTags)) {}

        for (FeatureTag featureTag : featureTags) {
            Class<Feature> clazz = tagFeatures.get(featureTag.name());
            clazz.newInstance().init(featureTag.name());
        }

    }

    boolean orderTag(List<FeatureTag> unOrderTags) {
        Iterator<FeatureTag> itr =  unOrderTags.iterator();
        while (itr.hasNext()) {
            FeatureTag tag = itr.next();
            String[] tags = tag.dependencies();
            List<FeatureTag> tagList = Arrays.asList(tags).stream().map(it -> featureTagByNameMap.get(it)).collect(Collectors.toList());
            if (featureTags.containsAll(tagList)) {
                featureTags.add(tag);
                itr.remove();
            }
        }

        if (unOrderTags == null || unOrderTags.size() == 0) {
            return true;
        }

        return false;
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }
}
