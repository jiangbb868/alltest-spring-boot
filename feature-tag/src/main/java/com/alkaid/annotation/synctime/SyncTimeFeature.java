package com.alkaid.annotation.synctime;

import com.alkaid.annotation.annotation.FeatureTag;
import com.alkaid.annotation.base.AbstractFeature;

@FeatureTag(name ="sync-time", dependencies = {"feature-a","feature-b"})
public class SyncTimeFeature extends AbstractFeature {

    @Override
    public boolean initBusiness(String tagName) {
        System.out.println("feature sync-time initialize");

        /**
         * 1.将server_info中的服务器添加到时间同步服务器列表中
         * 2.将dcs_info中的服务器添加到时间同步服务器列表中
         * 3.创建定时任务
         */

        return true;
    }
}
