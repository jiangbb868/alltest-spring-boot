package com.alkaid.annotation.functiona;

import com.alkaid.annotation.annotation.FeatureTag;
import com.alkaid.annotation.base.AbstractFeature;

@FeatureTag(name ="feature-a")
public class FeatureA extends AbstractFeature {
    @Override
    public boolean initBusiness(String tagName) {
        System.out.println("feature a initialize");
        return true;
    }
}
