package com.alkaid.annotation.base;

public interface Feature {

    boolean init(String featureTag);

}
