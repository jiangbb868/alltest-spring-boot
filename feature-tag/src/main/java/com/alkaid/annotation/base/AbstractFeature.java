package com.alkaid.annotation.base;

import com.alkaid.annotation.annotation.FeatureTag;

@FeatureTag(name ="abstract-feature")
public abstract class AbstractFeature implements Feature {
    @Override
    public boolean init(String featureTag) {
        /**
         * 1.查询特性标签表，查询所有标签，存储于Map中。
         * 2.如果tagName在tag的Map中，则返回true。
         * 3.向标签表中添加本标签，版本号为当前版本，时间为当前时间。
         */

        initBusiness(featureTag);
        return true;
    }

    public abstract boolean initBusiness(String tagName);
}
