package com.alkaid.alltest.micrometer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MMApplication {
    private Logger logger = LoggerFactory.getLogger(MMApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(MMApplication.class, args);
    }
}
